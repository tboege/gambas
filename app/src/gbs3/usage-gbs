
Compile and execute a Gambas script.

Usage: gbs3 [options][--] [<script file> | - ]
       gbs3 --convert-project <input project directory> <output script directory>

Options:
  -b --buildonly           process and compile the script without executing it
  -c --nocache             force the script compilation (do not check cache)
  -e                       execute the source code provided by the command line ( ':' separator )
  -g --debug               add debugging information to application
  -f --fast                use just-in-time compiler
  -h --help                display this help
  -L --license             display license
  -S --strict              fail if 'Public' or 'Sub' are defined without a 'main' function
  -t --trace               turn on tracing option during execution
  -T --terse-listing       only print a very terse error report on compile errors
  -u --use <components>    force component loading ('comp1,comp2...')
  -U --unsafe              allows jit compiler to generate unsafe faster code
  -v --verbose             be verbose
  -V --version             display version
  -w --warnings            display warnings during compilation
  --convert-project        convert a simple project to a script 
  --                       stop any further option processing  
  -                        read the script from standard input
